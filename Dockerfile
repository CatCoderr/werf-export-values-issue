FROM gradle:7.3.1-jdk17 as cache
RUN mkdir -p /home/gradle/cache_home
ENV GRADLE_USER_HOME /home/gradle/cache_home

COPY . /app
WORKDIR /app

RUN gradle clean build

FROM gradle:7.3.1-jdk17 as builder

ARG MODULE_NAME

COPY --from=cache /home/gradle/cache_home /home/gradle/.gradle

COPY . /app
WORKDIR /app

RUN gradle $MODULE_NAME:build -i --stacktrace

FROM azul/zulu-openjdk:17

ARG MODULE_NAME

WORKDIR /app

COPY --from=builder /app/$MODULE_NAME/build/libs/$MODULE_NAME-1.0.0.jar ./app.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","./app.jar"]
